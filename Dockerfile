
# FROM ubuntu:20.04
FROM ubuntu:22.04 as builder


RUN apt-get update -y && \
    apt-get install -y git python3-pip


# We copy just the requirements.txt first to leverage Docker cache
COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip3 install -r requirements.txt --no-cache-dir

COPY . /app

##App

FROM ubuntu:22.04 as app

COPY --from=builder  /usr/local/lib/python3.10/dist-packages /usr/local/lib/python3.10/dist-packages
COPY --from=builder  /usr/local/bin/ /usr/local/bin/
COPY --from=builder /app/ /app/

RUN ls /app/

RUN apt-get update -y && \
    apt-get install -y python3 && \
    apt-get install -y --reinstall ca-certificates


ENV PATH=/root/.local/bin:$PATH

WORKDIR /app

ENTRYPOINT [ "python3" ]

CMD ["-m", "sucks_bot.bot" ]

