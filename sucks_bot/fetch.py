"""fetch.py -- code to fetch reviews."""

import urllib.request
import json
import random
import time
import textwrap

import requests
from bs4 import BeautifulSoup
import pathlib


URL_TEMPLATE = "https://store.steampowered.com/appreviews/{game_id}?json=1&cursor={cursor}&review_type=negative&filter=updated"
FILE_NAME = "./sucks.txt"
GAME_ID = 232430 #440

#Wait time is in seconds
WAIT_TIME = 60

WIDTH = 40


r = urllib.request.Request(URL_TEMPLATE)


def request_reviews(game_id=GAME_ID, review_limit=100):
    reviews = []
    cursor = "*"
    while cursor and len(reviews) < review_limit:
        url = URL_TEMPLATE.format(cursor=cursor, game_id=game_id)
        r = urllib.request.urlopen(url, timeout=5)
        r_contents = json.loads(r.read())

        cursor = r_contents.get('cursor')

        new_reviews = r_contents.get('reviews', [])
        if new_reviews:
            reviews += new_reviews
        else:
            break

    return reviews


def request_image(game_id=GAME_ID, file_cache="cache/") -> pathlib.Path:
    """Grab the main image for the game."""
    file_cache_path =  pathlib.Path(file_cache)

    if not file_cache_path.exists():
        file_cache_path.mkdir()


    header_req = requests.get(f"https://steamcdn-a.akamaihd.net/steam/apps/{game_id}/header.jpg")

    location = file_cache_path / (str(game_id) + ".jpeg")

    with open(location, "wb") as f:
        f.write(header_req.content)

    return location


def request_game_info(game_id=GAME_ID) -> dict:
    """Grabs information about a game."""

    url_template = "https://store.steampowered.com/api/appdetails?appids={game_id}"
    url = url_template.format(game_id=game_id)

    game_req = requests.get(url)

    game_data = game_req.json()

    try:
        return game_data[str(game_id)]
    except TypeError as e:
        print(f"Error fetching data for {game_id}.", e)

    except AttributeError as e:
        raise e


def countdown(total_time, halfway=True, countdown_from=5):

    first_countdown = total_time - countdown_from

    time.sleep(first_countdown)

    for n in range(countdown_from, 0, -1):
        print(f"Next quote in {n}s...")
        time.sleep(1)


def output_reviews(reviews):
    while True:
        shuffled = random.shuffle(reviews)

        for review in reviews:
            print("Switching quote...")
            with open(FILE_NAME, "w") as f:
                wrapped_review = textwrap.fill(review['review'],
                                               width=WIDTH)
                f.write(wrapped_review)

            print(f"Switched. Next quote in {WAIT_TIME}s")

            # time.sleep(WAIT_TIME)
            countdown(WAIT_TIME)
