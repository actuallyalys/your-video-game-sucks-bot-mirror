"""bot.py -- Core code for the bot."""

import logging
import sys
import traceback

import yaml
from mastodon import Mastodon

import sucks_bot.fetch as fetch
import sucks_bot.database as db


def create_connection_from_key(config: dict) -> Mastodon:
    """Creates a connection using a configuration dictionary."""

    mastodon = Mastodon(**config["instance"])

    return mastodon


def load_config() -> dict:
    """Load configuration."""

    with open("config/config.yaml") as f:
        config = yaml.safe_load(f.read())

    return config


def reverse_enumerate(iterator, start=None):
    """Same as enumerate, but counts backwards.

    If start is not provided, it uses len(iterator). If iterator doesn't
    implement __len__, raises a TypeError.
    """

    if start is None:
        try:
            start = len(iterator) - 1
        except TypeError as e:
            raise TypeError("start not provided and could not be calculated based on len." + str(e))

    for item in iterator:
        yield (start, item)

        start -= 1


def truncate_string(text: str, limit, truncation="…") -> str:

    if len(text) <= limit:
        return text

    #We hahve to find a stop point such that adding a truncation character
    #doesn't push us over the limit:
    start_point = limit - len(truncation)

    for i, char in reverse_enumerate(text[start_point:0]):

        if char in " \t\n":
            truncation_point = i
            break
    else:
        truncation_point = start_point

    return text[:truncation_point] + truncation


def print_help():
    print(f"""USAGE: {sys.argv[0]} COMMAND

    Commands:
    fetch - update database with new reviews
    post - post a single status
    init - creates the database
    status - show status of database""")

def get_context(full_text, substrings, context_length):
    contexts = []

    for substring in substrings:

        match_start = full_text.find(substring)

        match_end = match_start + len(substring)

        start = max(0, match_start-context_length)
        end = min(len(full_text), match_end+context_length+1)

        contexts.append(full_text[start:end])

    return contexts



if __name__ == '__main__':
    # if False:
    config = load_config()
    client = create_connection_from_key(config)
    character_limit = config.get('character_limit', 500)
    database = config.get('database')

    command = sys.argv[1].lower()

    if command == 'fetch':
        for game_id in config.get('game_ids', []):
            print(f"Fetching reviews for {game_id}...")
            reviews = fetch.request_reviews(game_id)
            review_texts = [(review['review'], review['votes_funny'])
                            for review in reviews]
            game_info = fetch.request_game_info(game_id)

            if not game_info:
                print(f"Unable to fetch metadata for {game_id}, retrying...")
                game_info = fetch.request_game_info(game_id)
            if not game_info:
                print(f"Unable to fetch metadata for {game_id}, skipping...")
                continue

            for review in reviews:
                # print(f"Inserting reviews for {game_id}...")
                blocked_words = [blocked_word for blocked_word in config.get('block', [])
                                 if (blocked_word in review['review'])]

                blocked_contexts = get_context(review['review'], blocked_words,
                                              10)

                if blocked_words:
                    print(f"WARNING: review contains the following blocked words: {','.join(blocked_words)}")

                    for context in blocked_contexts:
                        print(f"\t{context}")

                    print(f"Considering marking this review (ID {review['recommendationid']}) as not allowed")

                db.add_review_if_not_exists(database, review["recommendationid"],
                                            game_id, game_info['data']['name'],
                                            review['review'])
    elif command == 'post':
        character_limit = config.get('character_limit', 500)
        try:
            game_id, game_title, review_text, review_id = db.get_review(database)
            print(f"Posting review for {game_id} {game_title}")

            page_url = f"https://store.steampowered.com/app/{game_id}"
            downloaded_image = fetch.request_image(game_id)
            img = client.media_post(str(downloaded_image),
                                    description=f"Image of the game '{game_title}'")

            truncated_review = truncate_string(review_text, (character_limit - 1 - len(page_url)))

            client.status_post(f'"{truncated_review}"\n{page_url}', media_ids=[img])

            db.mark_review_posted(database, review_id)
        except ValueError as e:
            print("Could not post review.")
            print(e)
            traceback.print_exc()
    elif command == 'init':
        db.create_database(database)
    elif command == 'status':
        results = db.check(database)
        print(f"Total reviews:\t\t{results['total_reviews']}\nUnposted reviews:\t{results['unposted_reviews']}\nTotal games:\t\t{results['total_games']}")
    elif command == 'help':
        print_help()
    elif command:
        print(f"Command {command} not found")
        print_help()
    else:
        print("Command required.")
        print_help()

    # except Exception as e:
    #     print(e)
