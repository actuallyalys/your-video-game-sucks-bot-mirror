import datetime
import sqlite3


def create_database(db_address):
    """Creates a new database."""

    conn = sqlite3.connect(db_address)

    conn.execute("""CREATE TABLE reviews
                (id INTEGER PRIMARY KEY AUTOINCREMENT,
                game_id INTEGER,
                external_id INTEGER,
                review_text TEXT,
                added TEXT,
                posted INTEGER,
                allowed INTEGER)""")

    conn.execute("""CREATE TABLE games
                (id INTEGER,
                external_id INTEGER,
                game_title TEXT)""")

    conn.commit()

    return conn


def game_already_stored(db_address, external_id):
    """Checks whether a game with that ID already exists."""

    conn = sqlite3.connect(db_address)
    results = conn.execute("""SELECT id FROM games WHERE external_id = ?""",
                           (external_id,))

    return results.fetchone() is not None


def add_game(db_address, external_id, game_title):
    """Adds a game."""

    conn = sqlite3.connect(db_address)

    conn.execute("""INSERT INTO games (external_id, game_title)
                 VALUES (:external_id, :game_title)""",
                 {"external_id": external_id, "game_title": game_title})
    # i don't think we can rely on the commit statement for the review
    # committing this data because this is a separate connection
    conn.commit()


def add_game_if_not_exists(db_address, external_id, game_title):
    if not game_already_stored(db_address, external_id):
        return add_game(db_address, external_id, game_title)


def already_stored(db_address, external_id):
    """Checks whether a review with that ID already exists."""

    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT id FROM reviews WHERE external_id = ?""",
                           (external_id,))

    return results.fetchone() is not None


def add_review(db_address, external_id, game_id, review_text):
    """Adds a review to the database to be posted later."""

    conn = sqlite3.connect(db_address)
    now = datetime.datetime.now().isoformat()

    conn.execute("""INSERT INTO reviews
                 (game_id, external_id, review_text, added, posted, allowed)
                 VALUES (:game_id, :external_id, :review_text, :added, NULL, 1)""",
                 {"game_id": game_id, "external_id": external_id,
                  "review_text": review_text, "added": now})

    conn.commit()


def add_review_if_not_exists(db_address, external_id, game_id, game_title, review_text):
    add_game_if_not_exists(db_address, game_id, game_title)

    if not already_stored(db_address, external_id):
        return add_review(db_address, external_id, game_id, review_text)


# TODO Create a context manager version of this?
def get_review(db_address):
    """Grabs an unposted, allowed review.

    Does not mark the review posted—do that with mark_review_posted."""
    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT reviews.game_id, game_title, review_text, reviews.id
                              FROM reviews
                              LEFT OUTER JOIN games ON
                                (reviews.game_id = games.external_id)
                              WHERE posted IS NULL
                                AND allowed <> 0
                            ORDER BY RANDOM();""")

    print(conn)
    print(results)

    result = results.fetchone()

    if result:
        # print(results.rowcount)
        game_id, game_title, review_text, review_id = result

        return (game_id, game_title, review_text, review_id)

    else:
        return tuple()


def mark_review_posted(db_address, review_id: int):
    """Marks a review as just having been posted."""
    conn = sqlite3.connect(db_address)

    results = conn.execute("""UPDATE reviews
                            SET posted = 1
                            WHERE reviews.id = :review_id
                           """, {"review_id": review_id})

    conn.commit()
    return results


def check(db_address):
    conn = sqlite3.connect(db_address)

    review_count = conn.execute("""SELECT count(*) FROM reviews""").fetchone()[0]
    game_count = conn.execute("""SELECT count(*) FROM games""").fetchone()[0]

    unposted_count = conn.execute("""SELECT count(*)
                              FROM reviews
                              LEFT OUTER JOIN games ON
                                (reviews.game_id = games.id)
                              WHERE posted IS NULL
                                  AND allowed <> 0;""").fetchone()[0]

    return {"total_reviews": review_count, "unposted_reviews": unposted_count,
            "total_games": game_count}
